from os import mkdir, environ
environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "1"
from os.path import exists
from os.path import join
from baseclasses import EventHandler
import logging
import sys



LOGPATH = "log"
LOGLEVEL = logging.ERROR
LOGFORMAT = '%(asctime)s - %(levelname)s - %(message)s - %(module)s %(funcName)s'
log = logging.getLogger()


def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    log.fatal("Uncaught exception, instance terminated unexpectedly", exc_info=(exc_type, exc_value, exc_traceback))


def main():
    activity = EventHandler()
    activity.start()
    activity.join()


if __name__ == "__main__":
    if not exists(LOGPATH) and LOGLEVEL <= logging.CRITICAL:
        mkdir(LOGPATH)

    if LOGLEVEL <= logging.CRITICAL:
        logging.basicConfig(level=LOGLEVEL, filename=join(LOGPATH, 'app.log'), filemode='a', format=LOGFORMAT)
    
    sys.excepthook = handle_exception
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(LOGLEVEL)
    handler.setFormatter(logging.Formatter(LOGFORMAT))
    log.addHandler(handler)
    main()