set app_name="app"

pyinstaller --log-level ERROR --clean ^
    --noconfirm --onefile --windowed ^
    --add-data="assets" ^
    --icon=""
    --name "%app_name%" "main.py"