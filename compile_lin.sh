#!/bin/bash 

app_name="app"

pyinstaller \
        --log-level ERROR --clean \
        --noconfirm --onefile \
        --add-data="assets:."\
        --icon=""\
        --name "$app_name" "main.py"
