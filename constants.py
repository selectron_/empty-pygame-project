from pygame import NUMEVENTS
from pygame.event import custom_type, event_name


event_listeners = dict([(i, []) for i in range(NUMEVENTS) if event_name(i) != event_name(NUMEVENTS)])
'''add user events here'''
BLOCK_EVENT     = custom_type() #"btype": eventid, "exc": [obj1, obj2, ...]
ALLOW_EVENT     = custom_type() #"atype": eventid, "exc": [obj1, obj2, ...]

#colors
TRANS       = 0, 0, 0, 0
BLACK       = [0, 0, 0]
WHITE       = [255, 255, 255]
RED         = [255, 0, 0]
GREEN       = [0, 255, 0]
BLUE        = [0, 0, 255]
LIGHTBLUE   = [200, 200, 255]
YELLOW      = [255, 255, 0]
MAGENTA     = [255, 0, 255]
CYAN        = [0, 255, 255]
GREY        = [100, 100, 100]
LIGHTGREY   = [200, 200, 200]
DARKGREY    = [50, 50, 50]
ORANGE      = [255, 165, 0]