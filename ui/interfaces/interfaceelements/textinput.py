from constants import  WHITE, GREY, DARKGREY, BLOCK_EVENT, ALLOW_EVENT
from pygame import Surface, SRCALPHA, KEYDOWN, KEYUP, MOUSEBUTTONUP, K_RETURN, K_BACKSPACE, K_LEFT, K_RIGHT, K_HOME, K_END, K_DELETE
from pygame.event import Event, post
from pygame.font import SysFont
from ui.baseclasses import ActiveInterfaceElement
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from pygame import Vector2, Color
    from pygame.font import Font
    from ui.baseclasses import Interface


class TextInput(ActiveInterfaceElement):
    def __init__(self, interface:"Interface", pos:"Vector2", size:"Vector2", font:"Font", color:"Color"=WHITE) -> None:
        super().__init__(interface)

        self._add_event(KEYDOWN)
        self._add_event(MOUSEBUTTONUP)

        self.image = Surface(size, SRCALPHA)
        self.rect = self.image.get_rect(topleft = pos)

        self.font = font

        self.image_plain = self.image.copy()
        self.image_forbidden = self.image.copy()
        self.image_forbidden.fill(DARKGREY + [100])


        self.image = self.image_plain

        self.color = color
        self.cursor_pos = 0
        self.cursor_timer = 0
        self.cursor_visible = True
        self.cursor_image = Surface((2, self.font.get_linesize()))
        self.cursor_image.fill(self.color)
        self.cursor_rect = self.cursor_image.get_rect()

        self.text_input = ""
        self.text_render = self.font.render(self.text_input, True, self.color)
        self.new_text = False

    
    def onEvent(self, event:"Event"):
        super().onEvent(event)

        if self.forbidden: 
            return
        
        elif event.type == MOUSEBUTTONUP and event.type not in self.blocked_events:
            if self.rect.collidepoint(event.pos):
                self.active = True
                post(Event(BLOCK_EVENT, {"btype": KEYDOWN, "exc": [self]}))
                post(Event(BLOCK_EVENT, {"btype": KEYUP, "exc": [self]}))
            else:
                self.active = False
                post(Event(ALLOW_EVENT, {"atype": KEYDOWN, "exc": []}))
                post(Event(ALLOW_EVENT, {"atype": KEYUP, "exc": []}))
        
        elif event.type == KEYDOWN and self.active and event.type not in self.blocked_events:
            if event.key == K_RETURN:
                print("Entered text:", self.text_input)
                self.text_input = ""
                self.cursor_pos = 0
                self.new_text = True
            elif event.key == K_BACKSPACE:
                self.text_input = self.text_input[:max(0, self.cursor_pos - 1)] + self.text_input[self.cursor_pos:]
                self.cursor_pos = max(0, self.cursor_pos - 1)
                self.new_text = True
            elif event.key == K_LEFT:
                self.cursor_pos = max(0, self.cursor_pos - 1)
            elif event.key == K_RIGHT:
                self.cursor_pos = min(len(self.text_input), self.cursor_pos + 1)
            elif event.key == K_HOME:
                self.cursor_pos = 0
            elif event.key == K_END:
                self.cursor_pos = len(self.text_input)
            elif event.key == K_DELETE:
                self.text_input = self.text_input[:self.cursor_pos] + self.text_input[self.cursor_pos + 1:]
                self.new_text = True

            elif event.unicode:
                self.new_text = True
                self.text_input = self.text_input[:self.cursor_pos] + event.unicode + self.text_input[self.cursor_pos:]
                self.cursor_pos += len(event.unicode)
        return False


    def update(self) -> None:
        if self.forbidden:
            self.image = self.image_forbidden
            return 
        else:
            self.image = self.image_plain
        
        self.image.fill(GREY + [100])
        self.cursor_timer += 1
        if self.cursor_timer >= 30:
            self.cursor_timer = 0
            self.cursor_visible = not self.cursor_visible

        if self.new_text:
            self.new_text = False
            self.text_render = self.font.render(self.text_input, True, self.color)

        self.image.blit(self.text_render, (0, 0))
        if self.cursor_visible and self.active:
            self.cursor_rect.left = self.rect.left + self.font.size(self.text_input[:self.cursor_pos])[0]
            self.image.blit(self.cursor_image, self.cursor_rect)

