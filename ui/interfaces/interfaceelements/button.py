from constants import WHITE, BLACK
from pygame import Surface, SRCALPHA, MOUSEBUTTONDOWN, MOUSEBUTTONUP, MOUSEMOTION
from pygame.font import SysFont
from ui.baseclasses import ActiveInterfaceElement
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from pygame.event import Event
    from pygame import Vector2
    from ui.baseclasses import Interface


class Button(ActiveInterfaceElement):
    def __init__(self, interface:"Interface", pos:"Vector2", size:"Vector2", text:str = "", callback = None) -> None:
        super().__init__(interface)
        self.image = Surface(size, SRCALPHA)
        self.rect = self.image.get_rect(topleft = pos)

        self._add_event(MOUSEMOTION)
        self._add_event(MOUSEBUTTONDOWN)
        self._add_event(MOUSEBUTTONUP)

        self.callback = callback

        fontsize = 100
        font = SysFont(None, fontsize)
        while font.get_linesize() > self.rect.height/4:
            fontsize -= 1
            font = SysFont(None, fontsize)

        text_render = font.render(text, True, WHITE)
        text_rect = text_render.get_rect(center = (self.rect.w/2, self.rect.h/2))
        self.image_plain = self.image.copy()
        self.image_plain.fill(WHITE + [50])
        self.image_plain.blit(text_render, text_rect)
        self.image_highlighted = self.image.copy()
        self.image_highlighted.fill(WHITE + [100])
        self.image_highlighted.blit(text_render, text_rect)
        self.image_depressed = self.image.copy()
        self.image_depressed.fill(BLACK + [200])
        self.image_depressed.blit(text_render, text_rect)

        self.image = self.image_plain

    
    def onEvent(self, event:"Event"):
        super().onEvent(event)

        if self.forbidden: 
            return
        
        elif event.type == MOUSEBUTTONDOWN and not self in self.blocked_events:
            if self.rect.collidepoint(event.pos):
                self.image = self.image_depressed

        elif event.type == MOUSEBUTTONUP and not self in self.blocked_events:
            if self.rect.collidepoint(event.pos):
                self.image = self.image_highlighted
                if self.callback:
                    self.callback()
                else:
                    self.active = True

        elif event.type == MOUSEMOTION and not self in self.blocked_events:
            if self.rect.collidepoint(event.pos):
                self.image = self.image_highlighted
            else:
                self.image = self.image_plain


    def update(self) -> None:
        if self.forbidden:
            self.image = self.image_depressed