from constants import WHITE
from ui.baseclasses import PassiveInterfaceElement
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from pygame.sprite import Group
    from pygame.font import Font
    from pygame import Vector2

class Label(PassiveInterfaceElement):
    def __init__(self, interface: "Group", pos:"Vector2", font:"Font", text:str="", color=WHITE) -> None:
        super().__init__(interface)
        self.pos = pos
        self.font = font
        self.color = color
        self.text = None

        self.updateText(text)

    
    def updateText(self, text:str):
        if text == self.text:
            return
        
        self.text = text
        self.image = self.font.render(text, True, self.color)
        self.rect = self.image.get_rect(topleft=self.pos)