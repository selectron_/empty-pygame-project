from pygame.rect import Rect
from pygame.surface import Surface
from constants import BLACK
from pygame.sprite import  AbstractGroup
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from pygame import Surface, Rect

class Interface(AbstractGroup):
    def __init__(self) -> None:
        super().__init__()
        self.active = False
        self.rect:"Rect" = None
        self.bg_color = BLACK + [200]


    def draw(self, surface: Surface, bgsurf: Surface | None = None, special_flags: int = 0) -> list[Rect]:
        surface.fill(self.bg_color, self.rect)
        return super().draw(surface, bgsurf, special_flags)