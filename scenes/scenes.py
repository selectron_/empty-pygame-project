from constants import BLACK, GREEN, event_listeners
from .baseclasses import Scene
import pygame as pg
import logging
import threading

from .mainscene import MainScene
from .loadingscreen import LoadingScreen
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from pygame import Surface
    from pygame.event import Event
    from baseclasses.mainloop import MainLoop

log = logging.getLogger()


def load_scenes(scenes:"Scenes", init_done:threading.Semaphore, loading_done:threading.Semaphore):
    obj1 = MainScene(scenes, init_done)
    scenes.scenes.append(obj1)
    loading_done.release()

class Scenes():
    def __init__(self, thread:"MainLoop", surface:"Surface", init_done:threading.Semaphore) -> None:
        if surface is None and (surface := pg.display.get_surface()) is None:
            log.fatal(f"display not set")
            raise RuntimeError(f"display not set")

        self.thread = thread
        event_listeners[pg.QUIT].append(self)
        event_listeners[pg.KEYDOWN].append(self)

        pg.mouse.set_cursor(pg.SYSTEM_CURSOR_CROSSHAIR)
        self.surface = pg.display.set_mode(flags=pg.FULLSCREEN)
        self.rect = self.surface.get_rect()

        self.fps = pg.time.Clock()
        self.ups = pg.time.Clock()
        pg.font.init()
        self.font = pg.font.SysFont(None, 20)
        self.fontheight = self.font.get_linesize()
        self.fps_pos = (self.rect.width - self.font.size("999 FPS")[0], 0)
        self.ups_pos = (self.rect.width - self.font.size("999 UPS")[0], self.fontheight)

        self.f1_render = self.font.render("F1: debug overlay", True, GREEN)
        self.f2_render = self.font.render("F2: next scene", True, GREEN)
        self.f3_render = self.font.render("F3: pause", True, GREEN)
        self.f4_render = self.font.render("F4: exit", True, GREEN)

        self.f1_pos = 0, 0
        self.f2_pos = 0, self.fontheight * 1
        self.f3_pos = 0, self.fontheight * 2
        self.f4_pos = 0, self.fontheight * 3
        self.show_debug = True

        self.pause = False

        self.scenes:        'list[Scene]' = []
        self.scene_history: 'list[Scene]' = []
        
        loading_done = threading.Semaphore(value=0)
        self.scene_loader = threading.Thread(target=load_scenes, args=(self, init_done, loading_done))
        self.scene_loader.start()

        self.active_scene = LoadingScreen(self, loading_done)
        self.scenes.append(self.active_scene)


    def nextScene(self) -> None:
        self.scene_history.append(self.active_scene)
        self.active_scene = self.scenes[(self.scenes.index(self.active_scene) + 1) % len(self.scenes)]

    
    def prevScene(self) -> None:
        if len(self.scene_history) > 0:
            self.active_scene = self.scene_history.pop()


    def switchScene(self, scene:"Scene") -> None:
        self.scene_history.append(self.active_scene)
        self.active_scene = scene


    def onEvent(self, event:"Event") -> None:
    
        if event.type == pg.QUIT:
            self.thread.running = False
            pg.event.post(pg.event.Event(0))

        elif event.type == pg.KEYDOWN:
            match event.key:
                case pg.K_F1:
                    self.show_debug = not self.show_debug

                case pg.K_F2:
                    self.nextScene()

                case pg.K_F3:
                    self.pause = not self.pause

                case pg.K_F4:
                    self.thread.running = False
                    pg.event.post(pg.event.Event(0))

                case pg.K_F5:
                    log.error(f"{pg.key.name(event.key)} has no function")

                case pg.K_F6:
                    log.error(f"{pg.key.name(event.key)} has no function")

                case pg.K_F7:
                    log.error(f"{pg.key.name(event.key)} has no function")

                case pg.K_F8:
                    log.error(f"{pg.key.name(event.key)} has no function")

                case pg.K_F9:
                    log.error(f"{pg.key.name(event.key)} has no function")

                case pg.K_F10:
                    log.error(f"{pg.key.name(event.key)} has no function")

                case pg.K_F11:
                    log.error(f"{pg.key.name(event.key)} has no function")

                case pg.K_F12:
                    log.error(f"{pg.key.name(event.key)} has no function")

                case pg.K_F13:
                    log.error(f"{pg.key.name(event.key)} has no function")

                case pg.K_F14:
                    log.error(f"{pg.key.name(event.key)} has no function")

                case pg.K_F15:
                    log.error(f"{pg.key.name(event.key)} has no function")


    def update(self) -> None:
        self.ups.tick()
        if self.pause: return

        self.active_scene.update()
        switch_to = self.active_scene.getSwitchTo()
        if switch_to is not None:
            self.scene_history.append(self.active_scene)
            self.active_scene = switch_to


    def draw(self) -> None:
        self.surface.fill(BLACK)
        self.active_scene.draw(self.surface)
        if self.show_debug:
            self.surface.blit(self.font.render(str(int(self.fps.get_fps())) + " FPS", False, GREEN), self.fps_pos)
            self.surface.blit(self.font.render(str(int(self.ups.get_fps())) + " UPS", False, GREEN), self.ups_pos)
            self.surface.blit(self.f1_render, self.f1_pos)
            self.surface.blit(self.f2_render, self.f2_pos)
            self.surface.blit(self.f3_render, self.f3_pos)
            self.surface.blit(self.f4_render, self.f4_pos)

        self.fps.tick()
        pg.display.update()