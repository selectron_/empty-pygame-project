from constants import WHITE
from pygame import Surface
from pygame.display import get_window_size
from pygame.draw import rect as drawRect
from random import randint
from .baseclasses import Scene
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from .scenes import Scenes
    from threading import Semaphore


class LoadingScreen(Scene):
    def __init__(self, scenes:"Scenes", loading_sem:"Semaphore") -> None:
        super().__init__(scenes)
        
        self.loading_sem = loading_sem
        self.loading_done = False
        width, height = get_window_size()
        self.bar = Surface((width/3, height/50))
        self.bar_rect = self.bar.get_rect(center = (width/2, height/2))

        self.seglength = int(self.bar_rect.width/100) - 2
        self.i = 0
    

    def update(self) -> None:
        if self.loading_sem.acquire(blocking=False):
            self.loading_sem.release()
            self.loading_done = True

        if (randint(0, 100) < 20 and self.i < 99) or self.loading_done:
            drawRect(self.bar, WHITE, (self.i * (self.seglength + 2), 0, self.seglength, self.bar_rect.height))
            self.i += 1

        if self.i > 99:
            self.scenes.scenes.remove(self)
            self.scenes.active_scene = self.scenes.scenes[0]


    
    def draw(self, surface: Surface) -> None:
        surface.blit(self.bar, self.bar_rect)
