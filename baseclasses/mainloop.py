from pygame.display import get_surface
from pygame.time import get_ticks, delay
import threading
from scenes.scenes import Scenes

MAX_UPS = 60
UPS_PERIOD = 1_000 / MAX_UPS

class MainLoop(threading.Thread):
    name="MainLoop"
    def __init__(self, init_done:threading.Semaphore) -> None:
        super().__init__()
        self.init_done = init_done


    def run(self) -> None:
        self.running = True

        if (surface := get_surface()) is None:
            raise RuntimeError("surface not set")

        scenes = Scenes(self, surface, self.init_done)
        
        update_count = 0
        frame_count = 0

        start_time = 0
        elapsed_time = 0
        sleep_time = 0

        start_time = get_ticks()
        while self.running:
            scenes.update()
            update_count += 1
            scenes.draw()
            frame_count += 1

            elapsed_time = get_ticks() - start_time
            sleep_time = update_count * UPS_PERIOD - elapsed_time

            if sleep_time > 0:
                delay(int(sleep_time))

            while sleep_time < 0 and update_count < MAX_UPS-1:
                scenes.update()
                update_count += 1
                elapsed_time = get_ticks() - start_time
                sleep_time = update_count * UPS_PERIOD - elapsed_time