from constants import event_listeners
from baseclasses import MainLoop
from pygame.locals import *
from pygame.event import wait as wait_for_event
from pygame.display import get_init as get_display_init, set_mode as init_display
import threading
import logging



        
class EventHandler(threading.Thread):
    name="MainActivity"
    def onCreate(self):
        if get_display_init():
            raise RuntimeError("display is already initialized")
        init_display()
        
        self.init_done = threading.Semaphore(0)
        self.log = logging.getLogger()
        self.mainloop = MainLoop(self.init_done)
        self.mainloop.start()

        while not self.init_done.acquire(blocking=False) and (event := wait_for_event()):
            if event.type == QUIT:
                self.mainloop.running = False
                return
        else:
            self.init_done.release()


    def onDestroy(self):
        pass


    def run(self) -> None:
        self.onCreate()
        
        while self.mainloop.is_alive() and (event := wait_for_event()):
            for observer in event_listeners[event.type]:
                observer.onEvent(event)

        self.onDestroy()
